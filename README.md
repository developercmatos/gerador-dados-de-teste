# gerador-dados-de-teste

## Exemplo de criação de gerenciador de dados consultando de um arquivo json com filtros opcionais

# Classe JsonUtils com métodos genericos para criação de arquivo json e leitura do arquivo

```java
public class JsonUtils {
    private static final Random random = new Random();
    public static <T> void gravarListaEmArquivoJson(List<T> lista, String nomeArquivo) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            mapper.writeValue(new File(nomeArquivo), lista);
            System.out.println("Arquivo JSON criado com sucesso!");
        } catch (IOException e) {
            System.err.println("Falha ao processar o arquivo JSON: " + e.getMessage());
            e.printStackTrace(); // Log da exceção para análise detalhada
        }
    }

    public static <T> Optional<T> lerObjetoDeArquivoJsonComFiltroOpcional(String nomeArquivo, TypeReference<List<T>> typeReference, Predicate<T> filtro) {
        ObjectMapper mapper = new ObjectMapper();
        try {
            // Deserializar a lista de objetos do arquivo JSON
            List<T> objetos = mapper.readValue(new File(nomeArquivo), typeReference);

            if (filtro == null) {
                // Retornar um objeto aleatório se o filtro for null
                return Optional.ofNullable(objetos.get(random.nextInt(objetos.size())));
            } else {
                // Aplicar o filtro e retornar o primeiro objeto que corresponde ao filtro, se houver
                return objetos.stream().filter(filtro).findFirst();
            }
        } catch (IOException e) {
            e.printStackTrace();
            return Optional.empty();
        }
    }

    public static <T> List<T> lerListaDeArquivoJsonComFiltro(String nomeArquivo, TypeReference<List<T>> typeReference, Predicate<T> filtro) {
        ObjectMapper mapper = new ObjectMapper();
        try {
            // Deserializar a lista de objetos do arquivo JSON
            List<T> objetos = mapper.readValue(new File(nomeArquivo), typeReference);

            if (filtro == null) {
                // Opção 1: Retornar a lista completa
                // return objetos;

                // Opção 2: Retornar uma lista contendo um objeto aleatório
                return Collections.singletonList(objetos.get(random.nextInt(objetos.size())));
            } else {
                // Aplicar o filtro e retornar uma lista de objetos que correspondem ao filtro
                return objetos.stream().filter(filtro).collect(Collectors.toList());
            }
        } catch (IOException e) {
            e.printStackTrace();
            // Em caso de erro, retornar uma lista vazia
            return Collections.emptyList();
        }
    }
}
```

# Classe do objeto pessoa a ser convertido pra lista de objetos json

``` java
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Pessoa {

    @JsonProperty("nome")
    private String nome;

    @JsonProperty("idade")
    private int idade;

    @JsonProperty("telefone")
    private String telefone;

    @JsonProperty("endereco")
    private String endereco;
}
```

# Classe de filtros opcionais de atributos da classe Pessoa

```java

public class FiltrosPessoa {

    public static Predicate<Pessoa> filtroPorNome(String nome) {
        return p -> p.getNome().equalsIgnoreCase(nome);
    }

    public static Predicate<Pessoa> filtroPorIdadeTelefone(int idade,String telefone) {
       return p -> p.getIdade() == idade && p.getTelefone().equals(telefone);
    }
}

```

# Classe Main com uso dos métodos genéricos para verificação funcional

```java

public class Main {

    public static void main(String[] args) {
        List<Pessoa> pessoas = List.of(
                new Pessoa("João Silva", 28, "(11) 98765-4321", "Rua das Flores, 123, São Paulo, SP"),
                new Pessoa("Maria Oliveira", 34, "(21) 97654-3210", "Avenida Atlântica, 456, Rio de Janeiro, RJ"),
                new Pessoa("Carlos Pereira", 45, "(31) 91234-5678", "Praça da Liberdade, 789, Belo Horizonte, MG"),
                new Pessoa("Ana Costa", 22, "(41) 99876-5432", "Rua XV de Novembro, 1011, Curitiba, PR"),
                new Pessoa("Lucas Martins", 37, "(51) 98765-4321", "Avenida Borges de Medeiros, 2022, Porto Alegre, RS"),
                new Pessoa("Lucas Silva", 37, "(51) 98765-4323", "Avenida Borges de Medeiros, 2022, Porto Alegre, RS")

        );

        gravarListaEmArquivoJson(pessoas, "src/main/resources/data_test/pessoas.json");

        TypeReference<List<Pessoa>> typeRef = new TypeReference<>(){};

        //Retornar com filtro preenchido o item buscado
        Optional<Pessoa> pessoa = JsonUtils.lerObjetoDeArquivoJsonComFiltro("src/main/resources/data_test/pessoas.json", typeRef,filtroPorNome("Maria Oliveira"));
        System.out.println("Meu objeto retornado com filtro: " + pessoa);

        //Retornar registro random com filtro null
        Optional<Pessoa> pessoa2 = JsonUtils.lerObjetoDeArquivoJsonComFiltro("src/main/resources/data_test/pessoas.json", typeRef,null);
        System.out.println("Meu objeto retornado random: " + pessoa2);

        //Retornar lista com filtro preenchido o item buscado
        List<Pessoa> pessoasFiltradas = JsonUtils.lerListaDeArquivoJsonComFiltro("src/main/resources/data_test/pessoas.json", typeRef, filtroPorIdadeTelefone(37,"(51) 98765-4321"));
        System.out.println("Meus objetos retornados com filtro: " + pessoasFiltradas);

        //Retornar lista com filtro preenchido o item buscado
        List<Pessoa> pessoasSemFiltro = JsonUtils.lerListaDeArquivoJsonComFiltro("src/main/resources/data_test/pessoas.json", typeRef, null);
        System.out.println("Meus objetos retornados sem filtro: " + pessoasSemFiltro);
    }
}

```


